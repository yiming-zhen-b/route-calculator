package com.example.routecalculator.Utilities;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.util.Log;
import android.widget.Toast;

import com.example.routecalculator.R;
import com.example.routecalculator.asynctask.FetchUrl;
import com.example.routecalculator.object.MapStatus;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Locale;

public class Utilities {
    private static final String TAG = "Utilities";

    private static final String POLYLINE_COLOR = "#FEA0A0";
    private static final String SELECTED_POLYLINE_COLOR = "#FF0000";
    private static final String LAT_LNG_FORMAT = "Lat: %1$.5f, Long: %2$.5f";
    private static final String DISTANCE_UNIT = "KM";

    private static final float MARKER_COLOR = BitmapDescriptorFactory.HUE_CYAN;
    private static final float SELECTED_MARKER_COLOR = BitmapDescriptorFactory.HUE_RED;

    private static final int POLY_LINE_OPTION_WIDTH = 10;

    public static final int ZOOM_STREET = 15;
    public static final int ZOOM_COUNTRY = 10;
    public static final long UPDATE_INTERVAL =  1000;    // milliseconds
    public static final long FASTEST_INTERVAL = 500;     //milliseconds
    public static final String KEY_ROUTES = "ROUTES";
    public static final String SELECTED_DISTANCE = "Selected: ";
    public static final String TOTAL_SELECTED_DISTANCE = "Total: ";
    public static final LatLng DEFAULT_COORDINATE = new LatLng(-37.768486, 175.270224);

    public static double calculateDistance(LatLng start, LatLng destination){
        Location start_location = new Location(start.toString());

        start_location.setLatitude(start.latitude);
        start_location.setLongitude(start.longitude);

        Location des_location = new Location(destination.toString());
        des_location.setLatitude(destination.latitude);
        des_location.setLongitude(destination.longitude);

        return formatDouble(start_location.distanceTo(des_location) * 0.001);
    }

    public static ArrayList<Polyline> drawAllPolyline(GoogleMap map, ArrayList<Polyline> savedPolyline){
        ArrayList<Polyline> newSavedPolyLine = new ArrayList<>();
        for(Polyline polyline : savedPolyline){
            newSavedPolyLine.add(createNewPolylineObject(map, polyline));
        }
        setSelectedPolyLine(newSavedPolyLine);
        return newSavedPolyLine;
    }

    private static Polyline createNewPolylineObject(GoogleMap mMap, Polyline oldPolyline) {
        PolylineOptions lineOptions = new PolylineOptions();
        lineOptions.addAll(oldPolyline.getPoints());
        return drawNewPolyLineOnMap(mMap, lineOptions);
    }

    public static Polyline drawNewPolyLineOnMap(GoogleMap mMap, PolylineOptions lineOptions){
        lineOptions.width(POLY_LINE_OPTION_WIDTH);
        Polyline poly = mMap.addPolyline(lineOptions);
        poly.setClickable(true);
        return poly;
    }

    public static void drawSelectedPolyLine(GoogleMap mMap, ArrayList<Polyline> mSavedPolyLine, Polyline oldPolyline){
//        int oldPolyIndex = mSavedPolyLine.indexOf(oldPolyline);
//        Log.d(TAG, "drawSelectedPolyLine index: " + oldPolyIndex);
//        Log.d(TAG, "drawSelectedPolyLine size: " + mSavedPolyLine.size());

        mSavedPolyLine.add(createNewPolylineObject(mMap, oldPolyline));
        mSavedPolyLine.remove(oldPolyline);
        oldPolyline.remove();

        setSelectedPolyLine(mSavedPolyLine);
    }

    public static double drawPolyline(GoogleMap map, MapStatus mapStatus, LatLng destination, Context context){
        LatLng start;
        double distance = 0.0;
        int savedMarkerSize = mapStatus.getSavedMarkers().size();
        if(savedMarkerSize == 0) return distance;
        if(savedMarkerSize == 1){
            if(mapStatus.getCurrentLocation(context) != null)
                start = mapStatus.getCurrentLocation(context);
            else return distance;
        }else{
            start = mapStatus.getMarker(savedMarkerSize - 2).getPosition();
        }
        Log.d("start", start.toString());
        Log.d("destination", destination.toString());
        FetchUrl FetchUrl = new FetchUrl(map, start, destination, mapStatus.getSavedPolyline(), context);
        FetchUrl.execute();
        distance = calculateDistance(start, destination);
        return distance;
    }

    public static void setSelectedPolyLine(ArrayList<Polyline> savedPolyline){
        for(Polyline pol : savedPolyline)
            pol.setColor(Color.parseColor(POLYLINE_COLOR));
        savedPolyline.get(savedPolyline.size() - 1).setColor(Color.parseColor(SELECTED_POLYLINE_COLOR));
    }

    //Add marker
    public static void addMarker(GoogleMap map, MapStatus mapStatus, LatLng latLng, Context context){
        Marker marker = map.addMarker(new MarkerOptions()
                .position(latLng)
                .title(context.getString(R.string.dropped_pin))
                .snippet(String.format(Locale.getDefault(), Utilities.LAT_LNG_FORMAT, latLng.latitude, latLng.longitude)));

        mapStatus.addMarkers(marker);
        marker.showInfoWindow();
        Utilities.setSelectedMarker(mapStatus.getSavedMarkers(), marker);
        map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, Utilities.ZOOM_STREET));
    }

    public static void setSelectedMarker(ArrayList<Marker> savedMarkers, Marker marker){
        for(Marker ma : savedMarkers){
            ma.setIcon(BitmapDescriptorFactory.defaultMarker(MARKER_COLOR));
        }
        marker.setIcon(BitmapDescriptorFactory.defaultMarker(SELECTED_MARKER_COLOR));
    }

    public static double formatDouble(double dou){
        return Double.parseDouble(new DecimalFormat("##.##").format(dou));
    }

    public static String getTextViewString(String key, double distance){
        return key + distance + " " + DISTANCE_UNIT;
    }

    public static void compareCurrentLocationTime(long currentLocationTime, Context context){
        if(currentLocationTime == 0) return;
        if((System.currentTimeMillis() - currentLocationTime) > (UPDATE_INTERVAL * 10))
            Toast.makeText(context, "This is your old location saved at: " + DateFormat.getInstance().format(currentLocationTime), Toast.LENGTH_SHORT).show();
    }
}
