package com.example.routecalculator.object;

import android.content.Context;

import com.example.routecalculator.Utilities.Utilities;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polyline;

import java.io.Serializable;
import java.util.ArrayList;

public class MapStatus implements Serializable {

    private double selectedDistance, totalSelectedDistance;
    private long currentLocationTime = 0;
    private transient LatLng currentLocation;
    private transient ArrayList<Marker> savedMarkers;
    private transient ArrayList<Polyline> savedPolyline;

    public MapStatus(LatLng currentL) {
        this.selectedDistance = 0.0;
        this.totalSelectedDistance = 0.0;
        this.currentLocation = currentL;
        this.savedMarkers = new ArrayList<Marker>();
        this.savedPolyline = new ArrayList<Polyline>();
    }

    public boolean hasData(){
        if(savedMarkers == null || savedPolyline == null) {
            this.selectedDistance = 0.0;
            this.totalSelectedDistance = 0.0;
            this.currentLocation = null;
            this.savedMarkers = new ArrayList<Marker>();
            this.savedPolyline = new ArrayList<Polyline>();
            return false;
        }
        return savedMarkers.size() != 0 || savedPolyline.size() != 0;
    }

    //Current Location
    public void setCurrentLocation(LatLng currentLocation) {
        this.currentLocation = currentLocation;
        currentLocationTime = System.currentTimeMillis();
    }


    public LatLng getCurrentLocation(Context context) {
        Utilities.compareCurrentLocationTime(currentLocationTime, context);
        return currentLocation;
    }

    //Distances
    public void setSelectedDistance(double selectedDistance) {
        this.selectedDistance = selectedDistance;
    }

    public void setTotalSelectedDistance(double totalSelectedDistance) {
        this.totalSelectedDistance = Utilities.formatDouble(this.totalSelectedDistance + totalSelectedDistance);
    }

    public double getSelectedDistance() {
        return selectedDistance;
    }

    public double getTotalSelectedDistance() {
        return totalSelectedDistance;
    }

    //Poly lines
    public ArrayList<Polyline> getSavedPolyline() {
        return savedPolyline;
    }

    public void setSavedPolyline(ArrayList<Polyline> polyline) {
        savedPolyline = new ArrayList<>(polyline);
    }

    //Markers
    public void addMarkers(Marker marker) {
        savedMarkers.add(marker);
    }

    public ArrayList<Marker> getSavedMarkers() {
        return savedMarkers;
    }

    public Marker getMarker(int i) {
        return savedMarkers.get(i);
    }

    public void clearSavedMarkers(){
        savedMarkers = new ArrayList<>();
    }

//    List<Data> routes = new ArrayList<Data>();
//    Set<Data> routesSet = new HashSet<Data>();
//
//    public MapStatus(List<Data> routes) {
//        this.routes = routes;
//    }
//
//    public void addNewRoute(Marker start, Marker destination, double distance, Polyline polyline){
//        routes.add(new Data(start, destination, distance, polyline));
//        routesSet.add(new Data(start, destination, distance, polyline));
//    }
//
//    public Marker getLastMarker(){
//        return routes.get(routes.size() - 1).getDestination();
//    }
//
//    public void setSelectRouteColor(Polyline polyline){
//        for(Data d : routes){
//            if(d.getPolyline().equals(polyline)){
//                polyline.setColor(Color.parseColor(Utilities.POLYLINE_SELECTED_COLOR));
//            }else{
//                d.getPolyline().setColor(Color.parseColor(Utilities.POLYLINE_COLOR));
//            }
//        }
//    }
//
//    public void setSelectRouteColor(Marker start){
//        for(Data d : routes){
//            if(d.getStart().equals(start)){
//                d.getStart().setIcon(BitmapDescriptorFactory.defaultMarker(Utilities.SELECTED_MARKER_COLOR));
//                d.getDestination().setIcon(BitmapDescriptorFactory.defaultMarker(Utilities.SELECTED_MARKER_COLOR));
//            }else{
//                d.getStart().setIcon(BitmapDescriptorFactory.defaultMarker(Utilities.MARKER_COLOR));
//                d.getDestination().setIcon(BitmapDescriptorFactory.defaultMarker(Utilities.MARKER_COLOR));
//            }
//        }
//    }
//
////    public double getDestination(Polyline polyline){
////
////    }
//
//    private class Data{
//        private Marker start = null;
//        private Marker destination = null;
//        private double distance;
//        private Polyline polyline = null;
//
//        public Marker getStart() {
//            return start;
//        }
//
//        public Marker getDestination() {
//            return destination;
//        }
//
//        public double getDistance() {
//            return distance;
//        }
//
//        public Polyline getPolyline() {
//            return polyline;
//        }
//
//        public Data(Polyline polyline) {
//            this.polyline = polyline;
//        }
//
//        public Data(Marker start) {
//            this.start = start;
//        }
//
//        public Data(Marker start, Marker destination, double distance, Polyline polyline) {
//            this.start = start;
//            this.destination = destination;
//            this.distance = distance;
//            this.polyline = polyline;
//        }
//
//        @Override
//        public int hashCode() {
//            if(polyline != null)
//                return polyline.hashCode();
//            if(start != null)
//                return start.hashCode();
//            return start.hashCode() ^ destination.hashCode();
//        }
//
//        @Override
//        public boolean equals(Object obj) {
//            Data data = (Data) obj;
//            if(data.polyline != null)
//                return data.polyline.equals(polyline);
//            if(data.start != null)
//                return data.start.equals(start);
//            return ((Data) obj).start.equals(start) && ((Data) obj).destination.equals(destination);
//        }
//    }
}
