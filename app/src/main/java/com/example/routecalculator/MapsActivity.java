package com.example.routecalculator;

import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.example.routecalculator.Utilities.Utilities;
import com.example.routecalculator.object.MapStatus;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.PointOfInterest;
import com.google.android.gms.maps.model.Polyline;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback{
    private static final String TAG = "MainActivity";
    private TextView distanceTextView;
    private TextView totalDistanceTextView;
    private FusedLocationProviderClient mFusedLocationClient;
    private GoogleMap mMap = null;
    private MapStatus mapStatus;
    private int MY_LOCATION_RC = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        Log.d(TAG, "onCreate is called");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        mapStatus = new MapStatus(null);
        distanceTextView = (TextView) findViewById(R.id.distance_text_view);
        totalDistanceTextView = (TextView) findViewById(R.id.total_distance_text_view);

        if(savedInstanceState != null && savedInstanceState.containsKey(Utilities.KEY_ROUTES)){
            mapStatus = (MapStatus)savedInstanceState.getSerializable(Utilities.KEY_ROUTES);
        }
        distanceTextView.setText(Utilities.getTextViewString(Utilities.SELECTED_DISTANCE, mapStatus.getSelectedDistance()));
        totalDistanceTextView.setText(Utilities.getTextViewString(Utilities.TOTAL_SELECTED_DISTANCE, mapStatus.getTotalSelectedDistance()));

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        requestPermission(MY_LOCATION_RC);
        retrieveMapStatus();
        //Map Long click
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                Utilities.addMarker(mMap, mapStatus, latLng, MapsActivity.this);
                mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                setTotalDistanceValueAndText(Utilities.drawPolyline(mMap, mapStatus, latLng, MapsActivity.this));
            }
        });
        //Route click
        mMap.setOnPolylineClickListener(new GoogleMap.OnPolylineClickListener()
        {
            @Override
            public void onPolylineClick(Polyline polyline)
            {
                List<LatLng> points = polyline.getPoints();
                setDistanceValueAndText(Utilities.calculateDistance(points.get(0), points.get(points.size() - 1)));
                Utilities.drawSelectedPolyLine(mMap, mapStatus.getSavedPolyline(), polyline);
            }
        });
        //Marker Click
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), Utilities.ZOOM_STREET));
                marker.showInfoWindow();
                Utilities.setSelectedMarker(mapStatus.getSavedMarkers(), marker);
                return true;
            }
        });
        //Set Poi Click
        mMap.setOnPoiClickListener(new GoogleMap.OnPoiClickListener() {
            @Override
            public void onPoiClick(PointOfInterest poi) {
                Marker poiMarker = mMap.addMarker(new MarkerOptions()
                        .position(poi.latLng)
                        .title(poi.name));
                poiMarker.showInfoWindow();
            }
        });
        //Override the setOnMyLocationButtonClickListener to check the GPS and permission after the user click the button.
        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                requestPermission(MY_LOCATION_RC);
                LatLng current = mapStatus.getCurrentLocation(MapsActivity.this);
                if(current != null){
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(current, Utilities.ZOOM_STREET));
                }
                return true;
            }
        });
    }

    private void retrieveMapStatus(){
        if(mapStatus.hasData()) {
            List<Marker> newSavedMarkers = new ArrayList<>(mapStatus.getSavedMarkers());
            mapStatus.clearSavedMarkers();
            for (Marker marker : newSavedMarkers) {
                Utilities.addMarker(mMap, mapStatus, marker.getPosition(), MapsActivity.this);
            }
            mapStatus.setSavedPolyline(Utilities.drawAllPolyline(mMap, mapStatus.getSavedPolyline()));
        }
    }


    private void removeMarker(){
        if(mMap != null){
            mMap.clear();
            LatLng current = mapStatus.getCurrentLocation(MapsActivity.this);
            if(current != null)
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(current, Utilities.ZOOM_STREET));
            mapStatus = new MapStatus(current);
            setTotalDistanceValueAndText(0.0);
        }
    }

    private void setDistanceValueAndText(double distance){
        mapStatus.setSelectedDistance(distance);
        distanceTextView.setText(Utilities.getTextViewString(Utilities.SELECTED_DISTANCE, distance));
    }

    private void setTotalDistanceValueAndText(double distance){
        setDistanceValueAndText(distance);
        mapStatus.setTotalSelectedDistance(distance);
        totalDistanceTextView.setText(Utilities.getTextViewString(Utilities.TOTAL_SELECTED_DISTANCE, mapStatus.getTotalSelectedDistance()));
    }

    LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            List<Location> locationList = locationResult.getLocations();
            if (locationList.size() > 0) {
                //The last location in the list is the newest
                Location location = locationList.get(locationList.size() - 1);
                LatLng current = new LatLng(location.getLatitude(), location.getLongitude());
                if(mapStatus.getCurrentLocation(MapsActivity.this) == null){
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(current, Utilities.ZOOM_STREET));
                }
                mapStatus.setCurrentLocation(current);
            }
        }
    };

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,  @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(grantResults.length == 2 && grantResults[0]== PackageManager.PERMISSION_GRANTED && grantResults[1]== PackageManager.PERMISSION_GRANTED){
            requestPermission(requestCode);
        }
        else {
            Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
            if(mMap != null) mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(Utilities.DEFAULT_COORDINATE, Utilities.ZOOM_COUNTRY));
        }
    }

    private void requestPermission(int requestCode){
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            checkGPS();
            if(requestCode == MY_LOCATION_RC){
                mMap.setMyLocationEnabled(true);
                LocationRequest mLocationRequest = LocationRequest.create()
                        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                        .setInterval(Utilities.UPDATE_INTERVAL)
                        .setFastestInterval(Utilities.FASTEST_INTERVAL);
                mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
            }
        }else{
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, requestCode);
        }
    }


    public void checkGPS(){
        LocationManager lm = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}

        if(!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage(getResources().getString(R.string.gps_network_not_enabled));
            dialog.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                }
            });
            dialog.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    Toast.makeText(getApplicationContext(), "GPS is disabled.", Toast.LENGTH_SHORT).show();
                }
            }).setCancelable(false);
            dialog.show();
        }
    }



    @Override
    protected void onResume() {
        super.onResume();
//        Log.d(TAG, "onResume is called");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
//        Log.d(TAG, "onSaveInstanceState is called");

        outState.putSerializable(Utilities.KEY_ROUTES, mapStatus);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.remove_marker:
                removeMarker();
                return true;
            case R.id.normal_map:
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                return true;
            case R.id.hybrid_map:
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                return true;
            case R.id.satellite_map:
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                return true;
            case R.id.terrain_map:
                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onStart() {
//        Log.d(TAG, "onStart is called");

        super.onStart();
    }

    @Override
    protected void onRestart() {
//        Log.d(TAG, "onRestart is called");
        if(mMap != null)
            requestPermission(MY_LOCATION_RC);
        super.onRestart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mFusedLocationClient != null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
    }
}

