package com.example.routecalculator.asynctask;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.routecalculator.object.JSONParserTask;
import com.example.routecalculator.R;
import com.example.routecalculator.Utilities.Utilities;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FetchUrl extends AsyncTask<String, Void, List<List<HashMap<String, String>>>> {
    private LatLng mStart, mDestination;
    private ArrayList<Polyline> mSavedPolyLine;
    private GoogleMap mMap;
    private Context context;

    public FetchUrl(GoogleMap map, LatLng start, LatLng destination, ArrayList<Polyline> savedPolyline, Context cont){
        mMap = map;
        mStart = start;
        mDestination = destination;
        mSavedPolyLine = savedPolyline;
        context = cont;
    }

    @Override
    protected List<List<HashMap<String, String>>> doInBackground(String... url) {
        List<List<HashMap<String, String>>> routes = null;
        try {
            String data = downloadUrl(buildQuery());
//            Log.d("Background Task data", data.toString());
            JSONObject jObject = new JSONObject(data);
//            Log.d("ParserTask", data.toString());
            JSONParserTask parser = new JSONParserTask();
//            Log.d("ParserTask", parser.toString());
            routes = parser.parse(jObject);
//            Log.d("ParserTask","Executing routes");
//            Log.d("ParserTask",routes.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return routes;
    }

    @Override
    protected void onPostExecute(List<List<HashMap<String, String>>> result) {
        super.onPostExecute(result);
        ArrayList<LatLng> points;
        PolylineOptions lineOptions = null;
        for (int i = 0; i < result.size(); i++) {
            points = new ArrayList<>();
            lineOptions = new PolylineOptions();
            List<HashMap<String, String>> path = result.get(i);
            for (int j = 0; j < path.size(); j++) {
                HashMap<String, String> point = path.get(j);
                double lat = Double.parseDouble(point.get("lat"));
                double lng = Double.parseDouble(point.get("lng"));
                LatLng position = new LatLng(lat, lng);
                points.add(position);
            }
            lineOptions.addAll(points);
//            Log.d("onPostExecute","onPostExecute lineoptions decoded");
        }
        if(lineOptions != null) {
            mSavedPolyLine.add(Utilities.drawNewPolyLineOnMap(mMap, lineOptions));
            Utilities.setSelectedPolyLine(mSavedPolyLine);
        }
        else {
            Log.d("onPostExecute","without Polylines drawn");
        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
//            Log.d("downloadUrl", data.toString());
            br.close();
        } catch (Exception e) {
           e.printStackTrace();
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    private String buildQuery(){
        String str_origin = "origin=" + mStart.latitude + "," + mStart.longitude;
        String str_dest = "destination=" + mDestination.latitude + "," + mDestination.longitude;
        String sensor = "sensor=false";
        String parameters = str_origin + "&" + str_dest + "&" + sensor;
        String output = "json";
        return "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + context.getString(R.string.google_maps_key);
    }
}